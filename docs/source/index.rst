The ANTARES Client
==================

Thank you for visiting the documentation for the ANTARES client. Begin with
:ref:`installation` and then see the :ref:`tutorial` for a quick introduction
and the :ref:`api` for a complete reference.

.. include:: contents.rst.inc
