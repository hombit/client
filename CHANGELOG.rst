Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`__

__ https://keepachangelog.com/en/1.0.0/

v1.0.1
------

Fixed
~~~~~

- Removed unnecessarily strict dependency requirements

v1.0.0
------

Added
~~~~~

- Type signatures for most of the library
- ``antares_client.search.cone_search`` for cone searches
- ``antares_client.search.get_by_ztf_object_id`` for lookup by ZTF Object ID

Changed
~~~~~~~

- Removed support for Python 3.4, 3.5
- Interfaces with the API at https://api.antares.noirlab.edu
- Streaming client returns loci instead of alerts
- Search queries hit API directly and no longer need to be prepared server-side

v0.3.2
------

Added
~~~~~

- Better error handling for networking issues in the Client.

v0.3.1
------

Fixed
~~~~~

- Use `time.perf_counter` instead of `time.process_time` for tracking
  timeout values. Fixes #1, where polling a Kafka stream took much
  longer than the specified timeout.

v0.3.0
------

Added
~~~~~

- The ``search`` subcommand, for searching and downloading querysets from the
  ANTARES ElasticSearch database.

- Started using ``click`` for CLI tooling.

- Initial release of documentation.

Fixed
~~~~~

- Verification of SSL certs in requests to the ANTARES portal for thumbnails.

Changed
~~~~~~~

- Renamed CLI ``antares-client`` to ``antares stream``.

v0.2.2
------

Added
~~~~~

- Support for custom Kafka commit behavior.

v0.2.0
------

Added
~~~~~

- ``antares_client.thumbnails`` module for downloading alert thumbnail images.

v0.1.0
------

Fixed
~~~~~

- \#6: ``_locate_ssl_certs_file`` was called in the ``Client`` constructor even
  if an SSL cert path was provided.

v0.0.1
------

Added
~~~~~

- Initial release
